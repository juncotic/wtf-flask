# - *- coding: utf- 8 - *-
from flask_wtf import FlaskForm #Importa funciones de formulario
from wtforms import StringField, TextField , HiddenField, PasswordField, TextAreaField, SelectField, RadioField,SubmitField #Importa campos
from wtforms.fields.html5 import EmailField,DateField #Importa campos HTML
from wtforms import validators #Importa validaciones
from wtforms_components import TimeField
from flask_wtf.file import FileField, FileRequired, FileAllowed #Importa funciones, validaciones y campos de archivo

#Clase de Registro
class Registro(FlaskForm):

    #Función de validación de nombre de usuario
    def nombre_usuario(form,field):
        #Verificar que no contenga guiones bajors o numeral
        if (field.data.find("_")!= -1) or (field.data.find("#")!= -1) :
            #Mostrar error de validación
             raise validators.ValidationError("El nombre de usuario solo puede contener letras, números y .")

    #Función para hacer campo opcional
    def opcional(field):
        field.validators.insert(0, validators.Optional())

    #Lista de opciones usada para select
    lista_opciones = [
        ('1','Opción 1'),
        ('2','Opción 2'),
        ('3','Opción 3'),
        ('4','Opción 4')
    ]

    #Lista de opciones usada para radio button
    lista_sexo = [
        ('f','Femenino'),
        ('m','Masculino'),
    ]

    #Definición de campo String
    nombre = StringField('Nombre',
    [
        #Definición de validaciones
        validators.Required(message = "Completar nombre")
    ])

    apellido = StringField('Apellido',
    [
        validators.Required(message = "Completar apellido")
    ])

    #Definición de campo fecha
    fechaNac = DateField('Fecha de Nacimiento',
    [
        validators.DataRequired(message="Ingrese una fecha válida")
    ])

    #Definición de campo hora
    hora = TimeField('Hora',
    [
        validators.DataRequired(message="Ingrese una hora válida")
    ])

    #Definición de campo de texto
    usuario = TextField('Usuario',
    [
        validators.Required(message = "Completar nombre de usuariousuario"),
        validators.length(min=4, max=25, message='La longitud del nombre de usuario no es válida'),
        #Validación definida por el usuario
        nombre_usuario
    ])

    #Definición de campo de contraseña
    password = PasswordField('Contraseña', [
        validators.Required(),
         #El campo de contraseña debe coincidir con el de confirmuar
        validators.EqualTo('confirmar', message='La contraseña no coincide')
    ])

    confirmar = PasswordField('Repetir contraseña')

    #Definición de campo de correo
    email = EmailField('Correo',
    [
        validators.Required(message = "Completar email"),
        validators.Email( message ='Formato de mail incorrecto')
    ])

    #Definición de campo de archivo
    imagen = FileField(validators=[
        FileRequired(),
        #Validación de tipo de archivo
        FileAllowed(['jpg', 'png'], 'El archivo debe ser una imagen jpg o png')
    ])

    bio = TextAreaField('Biografía')

    #Definición de campo select
    opciones = SelectField('Opción', choices=lista_opciones)

    #Definición de campo de radio
    sexo = RadioField('Sexo', choices=lista_sexo)

    #Definición de campo submit
    submit = SubmitField("Enviar")
